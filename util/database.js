const Sequelize = require('sequelize');


// Option 2: Passing parameters separately (other dialects)
const sequelize = new Sequelize('Department', 'root1', 'root1', {
  host: 'localhost',
  dialect: 'mysql'
})

try {
  // async()=>{
  sequelize.authenticate();
  console.log('Connection has been established successfully.');
  // }
} catch (error) {
  console.error('Unable to connect to the database:', error);
}

module.exports = sequelize;