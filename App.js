const express = require('express');
     const app = express();
      mysql = require('mysql');
      bodyParser = require('body-parser')
      sequelize = require('./util/database');
      deptroutes = require('./api/routes/department')
      employeeroutes = require('./api/routes/employee')
      salaryroutes = require('./api/routes/salary')

      app.use((req,res,next) => {
        res.header('Access-Control-Allow-Origin', '*')
        res.header('Access-Control-Allow-Headers', 'origin, X-Requested-With, Content-Type, Accept, Authorization')
        if(req.method === 'OPTIONS'){
            res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
            return res.status(200).json({})
        }
        next();
    });
app.use(bodyParser.json());
app.set("view engine", "ejs");
app.get("/abc",(res,req)=>{
  console.log("abc ----------------");
  
})
  sequelize.sync().then(result=>{
       console.log("All models were synchronized successfully.");
   })
   app.use(deptroutes);
   app.use(employeeroutes);
   app.use(salaryroutes);

// app.listen(3000,'192.168.0.142',()=>{
//   console.log("server is running on 3000");
  
// });


module.exports = app;