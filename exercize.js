var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "mydb"
});
var sql = "CREATE DATABASE mydb";
var table = "CREATE TABLE customers (name VARCHAR(255), address VARCHAR(255))";
var insert = "INSERT INTO customers (name, address) VALUES ?";
var show = "SELECT * FROM customers";
var where = "SELECT * FROM customers WHERE name='Viola'";
var like = "SELECT * FROM customers WHERE name LIKE '%o%'";
var address = 'Mountain 21';
var escape = 'SELECT * FROM customers WHERE name = ' + mysql.escape(address);
var sort = "SELECT * FROM customers ORDER BY name DESC";
var DELETE = "DELETE FROM customers WHERE address = 'Mountain 21'";
var DROP = "DROP TABLE customers";
var drop =  "DROP TABLE IF EXISTS customers ";
var update = "UPDATE customers SET address = 'Canyon 123' WHERE address = 'Sideway 1633'";



var values = [
  ['John', 'Highway 71'],
  ['Peter', 'Lowstreet 4'],
  ['Amy', 'Apple st 652'],
  ['Hannah', 'Mountain 21'],
  ['Michael', 'Valley 345'],
  ['Sandy', 'Ocean blvd 2'],
  ['Betty', 'Green Grass 1'],
  ['Richard', 'Sky st 331'],
  ['Susan', 'One way 98'],
  ['Vicky', 'Yellow Garden 2'],
  ['Ben', 'Park Lane 38'],
  ['William', 'Central st 954'],
  ['Chuck', 'Main Road 989'],
  ['Viola', 'Sideway 1633']
];
con.connect(function(err) {
  if (err) throw err;
  console.log("Updated!");
  con.query(where,function (err, result) {
    if (err) throw err;
    console.log("Result: " + JSON.stringify(result));
  });
});