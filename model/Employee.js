const Sequelize = require('sequelize');
const sequelize = require('../util/database')

//===========DEFINING DEPARTMENT MODEL============================================//

const Employee = sequelize.define('department', {
  // Model attributes are defined here
    id : {
      type:Sequelize.INTEGER,
      autoIncrement:true,
      allowNull:false,
      primaryKey:true
  },
    Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Emp_no:{
    type:Sequelize.INTEGER,
    autoIncrement:false,
    allowNull:false,
    primaryKey:false
  },
  Dept_id:{
    type:Sequelize.INTEGER,
    allowNull:false,
    references:{
      model:'departments',
       Key:'id'
      }
  },
    join_date: {
    type: Sequelize.DATEONLY 
  },
  end_date: {
    type: Sequelize.DATEONLY 
  }
}, {
  // Other model options go here
  tableName: 'Employee'
});
// `sequelize.define` also returns the model
console.log(Employee === sequelize.models.Employee); // true

module.exports = Employee;