const Sequelize = require('sequelize');
const sequelize = require('../util/database')

//===========DEFINING DEPARTMENT MODEL============================================//

const Salary = sequelize.define('department', {
  // Model attributes are defined here
    id : {
      type:Sequelize.INTEGER,
      autoIncrement:true,
      allowNull:false,
      primaryKey:true
  },
    Emp_id: {
      type:Sequelize.INTEGER,
      autoIncrement:false,
      allowNull:false,
      primaryKey:false
  },
  Month:{
    type:Sequelize.INTEGER,
    autoIncrement:false,
    allowNull:false,
    primaryKey:false
  },
  Year:{
    type:Sequelize.INTEGER,
    autoIncrement:false,
    allowNull:false,
    primaryKey:false
  },
    amount: {
      type:Sequelize.INTEGER,
      autoIncrement:false,
      allowNull:false,
      primaryKey:false 
  },
  Date: {
    type: Sequelize.DATEONLY,
    defaultValue: Sequelize.NOW()
       }
}, 
{
  // Other model options go here
  tableName: 'Salary'
});
// `sequelize.define` also returns the model
console.log(Salary === sequelize.models.Salary); // true

module.exports = Salary;