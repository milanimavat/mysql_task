const Sequelize = require('sequelize');
      sequelize = require('../util/database')

//===========DEFINING DEPARTMENT MODEL============================================//

const department = sequelize.define('department', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  Name: {
    type: Sequelize.STRING,
    allowNull: false
  },
  Date: {
    type: Sequelize.DATEONLY,
    defaultValue: Sequelize.NOW()
  }
}, {
  tableName: 'departments'
});

console.log(department === sequelize.models.department); // true

module.exports = department;