const express = require('express');
      router = express.Router();

      app = express();
      deptController = require('../controllers/controllerDEPT');

router.get("/department",deptController.show_dept_home);
router.post("/department/add",deptController.create_dept);
router.delete("/department/remove/:id",deptController.remove_dept)
router.put("/department/update/:id",deptController.update_dept)

module.exports = router;