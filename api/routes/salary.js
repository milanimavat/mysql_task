const express = require('express');
      router = express.Router();
      app = express();
      salaryController = require('../controllers/controllerSalary');

router.get("/salary", salaryController.show_salary_home);
router.post("/salary/add", salaryController.create_salary);
router.delete("/salary/remove/:id", salaryController.remove_salary);
router.put("/salary/update/:id", salaryController.update_salary);

module.exports = router;