const express = require('express');
      router = express.Router();
      app = express();
      employeeController = require('../controllers/controllerEMP');

router.get("/employee",employeeController.show_employee_home);
router.post("/employee/add",employeeController.create_employee);
router.delete("/employee/remove/:id",employeeController.remove_employee);
router.put("/employee/update/:id",employeeController.update_employee);

module.exports = router;