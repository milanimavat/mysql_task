const employee = require('../../model/Employee');
Sequelize = require('sequelize');
sequelize = require('../../util/database');

exports.show_employee_home = async (req, res) => {
  employee.findAll().then(result => {
    console.log(result);result
    res.send(JSON.stringify(result));
  })
}

exports.create_employee = async (req, res) => {
      if (req.body.Name === undefined || req.body.Emp_no === undefined || req.body.Dept_id === undefined) {
        return res.status(400).send({
          message: `neccessary fields are required ('Name','Emp_no','Dept_id')`
        })
      } else {
        employee.create({
            Name: req.body.Name,
            Emp_no: req.body.Emp_no,
            Dept_id: req.body.Dept_id,
            join_date: req.body.join_date,
            end_date: req.body.end_date
          }).then(result => {
            console.log(result);
            res.status(200).send({
              message: `Employee has been created successfully and the id :: ${result.id}`
            })
          })
          .catch(err => {
            console.log(err);
            res.status(400).send({
              message: "error in creating employee"
            })
          })
        }
}

        exports.remove_employee = async (req, res) => {

          employee.findByPk(req.params.id).then(result => {
              result.destroy();
              res.status(200).send({
                message: `Employee has been removed successfully and the id::${req.params.id}`
              })

            })
            .catch(err => {
              console.log(err);
              res.status(400).send({
                message: `error in deleting employee and the id::${req.params.id}`
              })
            })
        }

        exports.update_employee = async (req, res) => {

          employee.findByPk(req.params.id).then(result => {

            result.Name = req.body.Name;
            result.Emp_no = req.body.Emp_no;
            result.Dept_id = req.body.Dept_id;
            result.join_date = req.body.join_date;
            result.end_date = req.body.end_date;

            if (result.Name == undefined || result.Emp_no == undefined || result.Dept_id == undefined) {
              return res.status(400).send({
                message: `neccessary fields are required ('Name','Emp_no','Dept_id') and the id::${req.params.id}`
              })

            } else {
              let flag = result.save();
              console.log("comes into successsssssssssssss");
              res.status(200).send({
                message: `Employee has been updated successfully and the id::${req.params.id}`
              })
              return flag;
            }
          }).catch(err => {
            console.log(err);
            res.status(400).send({
              message: `error in updating employee and the id::${req.params.id}`
            })
          })

        }