const department = require('../../model/department');
Sequelize = require('sequelize');
sequelize = require('../../util/database');

exports.show_dept_home = async (req, res) => {
  department.findAll().then(result => {
    // console.log(result);
    res.send(JSON.stringify(result));
  })
}

exports.create_dept = async (req, res) => {
  if (req.body.Name === undefined) {
    return res.status(400).send({
      message: `neccessary fields are required ('Name')`
    })
  } else {
  department.create({
      Name: req.body.Name,
      // Date: req.body.Date
    }).then(result => {
      console.log('result', result);
      console.log(result.id);
      res.status(200).send({message:`Department has been created successfully and the id :: ${result.id}`})
    })
    .catch(err => {
      console.log(err);
      res.status(400).send({message:"error in creating department"})
    })
  }
}

exports.remove_dept = async (req, res) => {

  department.findByPk(req.params.id).then(result => {
      result.destroy();
      console.log("deleted result is-----------------------", result);
      res.status(200).send({message:`Department has been removed successfully and the id::${req.params.id}`})

    })
    .catch(err => {
      console.log(err);
      res.status(400).send({message:`error in deleting department and the id::${req.params.id}`})

    })
}

exports.update_dept = async (req, res) => {

  department.findByPk(req.params.id).then(result => {
    result.Name = req.body.Name; //req.body.Name
    if(!result.Name==undefined){
    let flag = result.save();
    console.log("dept is updated and id is::",result.id);
    res.status(200).send({message:`Department has been updated successfully and the id::${req.params.id}`})
    return flag;
    }
    else{
      return res.status(400).send({
        message: `neccessary fields are required ('Name') and the id::${req.params.id}`
      })
    }
    
  }).catch(err=>{
    console.log(err);
    // console.log("dept is not update and id is::",result.id);
    res.status(400).send({message:`error in updating department and the id::${req.params.id}`})
  })
}