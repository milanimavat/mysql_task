const salary = require('../../model/salary');
Sequelize = require('sequelize');
sequelize = require('../../util/database');

exports.show_salary_home = async (req, res) => {

  salary.findAll().then(result => {
    res.send(JSON.stringify(result));
  })
}

exports.create_salary = async (req, res) => {
  if (req.body.Emp_id === undefined || req.body.Month === undefined || req.body.Year === undefined || req.body.amount === undefined) {
    return res.status(400).send({
      message: `neccessary fields are required ('Emp_id','Month','Year','amount')`
    })
  } else {
  salary.create({
      Emp_id: req.body.Emp_id,
      Month: req.body.Month,
      Year: req.body.Year,
      amount: req.body.amount
      // Date: req.body.Date
    }).then(result => {
      console.log(result);
      res.status(200).send({message:`Salary has been created successfully and the id :: ${result.id}`})
    })
    .catch(err => {
      console.log(err);
      res.status(400).send({message:"error in creating salary"})

    })
  }
}

exports.remove_salary = async (req, res) => {

  salary.findByPk(req.params.id).then(result => {
      result.destroy();
      console.log("deleted result is-----------------------", result);
      res.status(200).send({message:`Salary has been removed successfully and the id::${req.params.id}`})
    })
    .catch(err => {
      console.log(err);
      res.status(400).send({message:`error in deleting salary and the id::${req.params.id}`})


    })
}

exports.update_salary = async (req, res) => {

  salary.findByPk(req.params.id).then(result => {
    console.log(result.Emp_id,req.body.Emp_id);
    
    result.Emp_id = req.body.Emp_id;
    result.Month = req.body.Month;
    result.Year = req.body.Year;
    result.amount = req.body.amount;

    if (result.Emp_id === undefined || result.Month === undefined || result.Year === undefined || result.amount === undefined) {

      return res.status(400).send({
        message: `neccessary fields are required ('Emp_id','Month','Year','amount') and the id::${req.params.id}`
      })

    } else {

      let flag = result.save();
      console.log("comes into successsssssssssssss");
      res.status(200).send({
        message: `Salary has been updated successfully and the id::${req.params.id}`
      })
      return flag;
    }
  }).catch(err=>{
    console.log(err);
    res.status(400).send({message:`error in updating salary and the id::${req.params.id}`})
  })

}